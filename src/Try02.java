/**
 * Fichero: Try02.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Try02 {

  public static void main(String[] args) {
    int num1 = 2, num2 = 0;

    try {
      System.out.println("El cociente es: " + (num1 / num2));
    } catch (ArithmeticException ae) {
      System.err.println("Error Aritmetico: " + ae.getMessage());
    } catch (Exception e) {
      System.err.println("Error: " + e.getMessage());
    } finally {
      System.err.println("Fin");
    }

  }
}

/* EJECUCION:
Error Aritmetico: / by zero
Fin
*/
