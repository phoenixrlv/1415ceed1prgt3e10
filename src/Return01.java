/**
 * Fichero: Return01.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Return01 {

  public void numeros() {
    int i = 1;

    while (i < 10) {
      System.out.print(i + " ");
      i++;
      if (i > 5) {
        return;
      }
    }
    return;
  }

  public static void main(String args[]) {
    Return01 r = new Return01();
    r.numeros();
  }
}

/* EJECUCION:
 1 2 3 4 5
 */
