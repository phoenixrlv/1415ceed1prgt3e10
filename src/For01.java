/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Fichero: For01.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class For01 {

  public static void main(String args[]) {

    for (int i = 1; i <= 5; i++) {
      System.out.print(i + " ");
    }
  }
}

/* EJECUCION:
 1 2 3 4 5
 */
