

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Fichero: DoWhile01.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class DoWhile02 {

  public static int leernumero() {
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);

    String linea = "";

    try {
      linea = buffer.readLine();

    } catch (IOException | NumberFormatException e) {
    }
    return Integer.parseInt(linea);

  }

  public static void main(String args[]) {
    int opcion = 0;

    do {
      System.out.println("MENU ");
      System.out.println("0. SALIR ");
      System.out.println("1. CASO A ");
      System.out.println("2. CASO B ");
      System.out.print("Introduce opción: ");
      opcion = leernumero();

      switch (opcion) {
        case 1:
          System.out.println("CASO A ");
          break;
        case 2:
          System.out.println("CASO B ");
          break;
      }

    } while (opcion != 0);

  }
}

/* EJECUCION:
 MENU 
 0. SALIR 
 1. CASO A 
 2. CASO B 
 Introduce opción: 1
 CASO A 
 MENU 
 0. SALIR 
 1. CASO A 
 2. CASO B 
 Introduce opción: 0
 */
